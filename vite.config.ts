import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers';
import * as path from 'path';
import liveReload from 'vite-plugin-live-reload'


// https://vitejs.dev/config/
export default defineConfig({
    server: {
        host: 'localhost',
        port: 8881,
        strictPort: true,
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
    },
    plugins: [
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
        vue({reactivityTransform: true,}),
        liveReload('./src/**/*.(js|vue)'),
    ],
})
