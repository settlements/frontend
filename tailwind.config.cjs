/** @type {import('tailwindcss').Config} */
module.exports = {
    corePlugins: {
        preflight: false,
    },
    prefix: 'tw-',
    content: ["./src/**/*.{vue,ts}"],
    theme: {
        extend: {
            colors: {
                login: '#F0F9FF',
                header: '#2B82E0',
                'card-border': '#409FFF',
                'card-border-hover': '#1765BF',
                'card-bg-hover': '#F0F9FF',
                'card-disabled': '#F1F3F5',
                'card-disabled-border-and-tag': '#909399',
                'video-none': '#D9D9D9',
                'video-play': '#003780',
            },
        },
    },
    plugins: [
        function ({addBase}) {
            addBase({
                ".el-button": {
                    "background-color": "var(--el-button-bg-color,var(--el-color-white))"
                }
            });
        }
    ]
}