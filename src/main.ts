import {createApp} from 'vue'
import App from './App.vue'
import router from "./router/router.js";
import {createPinia} from 'pinia';
import {defineRule} from 'vee-validate';
import AllRules from '@vee-validate/rules';
import './style.css';
import 'element-plus/es/components/message/style/css'
import 'element-plus/es/components/notification/style/css';
import vueFilePond from 'vue-filepond'
import 'filepond/dist/filepond.min.css'
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css'
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type'
import FilePondPluginImagePreview from 'filepond-plugin-image-preview'
import {labels_RU} from './helpers/filePondRus.js';
import {setOptions as setOptionFilePondLabel} from 'filepond';


const FilePond = vueFilePond(
    FilePondPluginFileValidateType,
    FilePondPluginImagePreview
)


const pinia = createPinia()

const app = createApp(App);
app.use(router);
app.use(pinia)
app.component('file-pond', FilePond)
app.mount('#app')


setOptionFilePondLabel({
    ...labels_RU
})
Object.keys(AllRules).forEach(rule => {
    defineRule(rule, AllRules[rule]);
});


