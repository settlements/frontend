import {api, setAuthorizationToken as setAuthorizationTokenApi } from './api.js';

export default {
  setAuthorizationToken(token) {
    setAuthorizationTokenApi(token);
  },
  async auth({login: email, password}) {
    const data = await api.get('/auth/login',
        {
          params: {email, password},
        });
    setAuthorizationTokenApi(data.data.access_token);
    return data.data;
  },
  async update(updateUserInformation) {
    const data = await api.put('/user/', {...updateUserInformation});
    return data.data;
  },
  async create(userInformation) {
    const data = await api.post('/user/', {...userInformation});
    return data.data;
  },
  async getById(id) {
    const data = await api.get('/user/byId', {
      params:{
        id
      }
    });
    return data.data;
  },
  async sortUpdate(sortType = 0) {
    const data = await api.post('/sort', {'sortType': sortType});
    return data.data;
  },
  async getUsers() {
    const data = await api.get('/user');
    return data.data;
  },
  async changePassword(id,password){
    const data = await api.post('/user/password', {'userId': id,password});
  },

};