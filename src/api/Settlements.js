import {api as baseApi} from './api.js';
import {BaseApiClass} from './BaseApiClass.js';


class  SettlementsApi extends BaseApiClass{
  async update (id,data){
    const res = await baseApi.put(`${this.baseUri}/${id}`,data);
    return res.data;
  }
  async create (data){
    const res = await baseApi.post(this.baseUri,data);

    return res.data;
  }
}


export const api = new SettlementsApi('settlements');