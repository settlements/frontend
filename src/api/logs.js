import {api} from './api.js';

export default {
  async getById(id) {
    const res = await api.get('/logs/byUserId', {
      params: {
        id,
      },
    });
    return res.data;

  },
  async getData() {
    const res = await api.get('/logs');
    return res.data;
  },
};