import {api as baseApi} from './api.js';
import {BaseApiClass} from './BaseApiClass.js';

class HousesApi extends BaseApiClass {
  async update(id, data) {
    const files = data.files;
    delete data.files;
    const res = await baseApi.put(`${this.baseUri}/${id}`, data);
    const gallery = await  this.gallerySave(id,files.gallery);
    console.log(files);
    return res.data;
  }

  async create(data) {
    const res = await baseApi.post(this.baseUri, data);

    return res.data;
  }


  async gallerySave (HouseId,files) {
    const res = await baseApi.post(`api/gallery/${HouseId}`, {
      house_id:HouseId,
      files : files
    },{
      headers:{
        'Content-Type': 'multipart/form-data'
      }
    });
  }
}


export const api = new HousesApi('houses');