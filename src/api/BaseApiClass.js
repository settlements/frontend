import {api} from './api.js';

export class BaseApiClass {
  constructor(uri) {
    this.baseUri = `/api/${uri}`;
  }

  async getById(id) {
    const res = await api.get(`${this.baseUri}/${id}`);
    return res.data;
  }

  async getData(filters) {
    const res = await api.get(this.baseUri,{
      params:filters
    });
    return res.data;
  }
}