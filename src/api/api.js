import axios from "axios";
import {userStore} from '../store/userStore.js';
import {ElNotification} from 'element-plus';
import router from '../router/router.js';
const userApi = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    headers:{
        common:{
        }
    }
});



const setAuthorizationToken = (token) => {
    if (token) {
        userApi.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
    else {
        delete userApi.defaults.headers.common['Authorization'];
    }
};


userApi.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if(error.response && error.response.status === 401){
        const store = userStore();
        store.setUserToken('');
        window.location.href ='/login';
    }
    let errorText =  error?.response?.data == undefined ? '' : error?.response?.data;
    if(error.response && error.response.status === 403){
        router.push({name: `home`});
        errorText = 'Данный материал станет вам доступен после изучения предыдущего!'
    }
    ElNotification({
        title: 'Ой-ей',
        message: `Произошла ошибка  ${errorText ? `:` : ''} ${errorText}` ,
        type: 'error',
    })
    return {data:''}
});




export    {userApi as api, setAuthorizationToken}

