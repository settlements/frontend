/** @format */
import {createRouter, createWebHistory} from 'vue-router';
import PageNotFound from '../views/PageNotFound.vue';
import Login from '../views/Login.vue';
import IndexAdmin from '../views/IndexAdmin.vue';

import AdminView from '../views/AdminView.vue';

import {settlements} from './settlements.js';
import {houses} from './houses.js';

// import {userStore} from '../store/userStore.js';

const routes = [
  {
    path: '/',
    component: Login,
    name: 'Login',
    meta: {
      requiresAuth: false,
      showMenu: false,
      title: 'Страница Авторизации',
    },
  },

  {
    path: '/admin',
    component: AdminView,
    name: 'adminPanel',
    meta: {
      requiresAuth: true,
      showMenu: false,
    },
    children: [
      {
        path: 'index',
        component: IndexAdmin,
        name: 'admin.index',
        meta: {
          requiresAuth: true,
          showMenu: true,
          title: 'Главная',
        },
      },
        ...houses,
        ...settlements
    ],

  },

  {
    path: '/:pathMatch(.*)*',
    component: PageNotFound,
    name: 'PageNotFound',
    meta: {
      templatePageHidden: true,
      showMenu: false,
      title: 'Страница не найдена',
    },
  },
];

console.log(routes)

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash
  // history for simplicity here.
  history: createWebHistory('/'),
  routes, // short for `routes: routes`
});

router.beforeEach(async (to, from, next) => {
  //pass validate function
  // const userStorage = userStore();
  // const hasToken = Boolean(userStorage?.user?.token);
  // if (hasToken) {
  //   if (Number(userStorage.user.meta.sort) === 0 && to.name !== 'sorting') {
  //     next({name: 'sorting'});
  //   }
  //   else {
  //     next();
  //   }
  // }
  // else if (!hasToken) {
  //   if (to.name === 'login') {
  //     next();
  //   }
  //   else {
  //     next({name: 'login'});
  //   }
  // }
  // else {
  //   next();
  // }
  next();
});

export default router;

