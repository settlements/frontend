
import Houses from '../views/House/Houses.vue';

import ChildrenView from '@/views/ChildrenView.vue';
import HouseCreate from '@/views/House/HouseCreate.vue';
import HouseEdit from '@/views/House/HouseEdit.vue';
import HouseView from '@/views/House/HouseView.vue';

export const houses = [
  {
    path: 'houses/all',
    component: Houses,
    name: 'admin.houses.all',
    meta: {
      requiresAuth: true,
      showMenu: true,
      title: 'Дома',
    },
  },
  {
    path: 'houses/create',
    component: HouseCreate,
    name: 'admin.house.create',
    requiresAuth: true,
    showMenu: false,
    title: 'Создать поселок',
  },
  {
    path: 'houses/:id',
    component: ChildrenView,
    name: 'admin.house',
    meta: {
      requiresAuth: true,
      showMenu: false,
    },
    children: [
      {
        path: 'edit',
        component: HouseEdit,
        name: 'admin.house.edit',
        requiresAuth: true,
        showMenu: false,
      },
      {
        path: 'view',
        component: HouseView,
        name: 'admin.house.view',
        requiresAuth: true,
        showMenu: false,
      }

    ],
  },
];