import Settlements from '@/views/Settlement/Settlements.vue';
import SettlementView from '@/views/Settlement/SettlementView.vue';
import ChildrenView from '@/views/ChildrenView.vue';
import SettlementEdit from '@/views/Settlement/SettlementEdit.vue';
import SettlementCreate from '@/views/Settlement/SettlementCreate.vue';

export const settlements = [
  {
    path: 'settlements/all',
    component: Settlements,
    name: 'admin.settlements.all',
    meta: {
      requiresAuth: true,
      showMenu: true,
      title: 'Поселки',
    },
  },
  {
    path: 'settlements/create',
    component: SettlementCreate,
    name: 'admin.settlement.create',
    requiresAuth: true,
    showMenu: false,
    title: 'Создать поселок',
  },
  {
    path: 'settlements/:id',
    component: ChildrenView,
    name: 'admin.settlement',
    meta: {
      requiresAuth: true,
      showMenu: false,
    },
    children: [
      {
        path: 'edit',
        component: SettlementEdit,
        name: 'admin.settlement.edit',
        requiresAuth: true,
        showMenu: false,
      },
      {
        path: 'view',
        component: SettlementView,
        name: 'admin.settlement.view',
        requiresAuth: true,
        showMenu: false,
      }

    ],
  },
];