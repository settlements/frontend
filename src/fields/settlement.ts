import {settlementFieldsInterface} from "../types/settlement";


export const settlementFields: settlementFieldsInterface[] = [
    {
        label: 'Название поселка',
        sysName: 'name',
        placeholder:'Можайск'
    },
    {
        label: 'Телефон дял связи',
        sysName: 'phone',
        placeholder:'256341376638'
    },
    {
        label: 'Адрес',
        sysName: 'address',
        placeholder:'256341376638'
    },
    {
        label: 'Площадь поселка',
        sysName: 'square',
        placeholder:'982.42'
    },
    {
        label: 'Площадь поселка',
        sysName: 'youtube',
        placeholder:'https://www.youtube.com/watch?v=9FlXKDHrUkC'
    }
]