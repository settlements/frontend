export const houseFields = [
    {
        label: 'Название',
        sysName: 'name',
        placeholder: 'Дом'
    },
    {
        label: ' Валюта по-умолчанию',
        sysName: 'currency',
        input: 'select'
    },
    {
        label: 'Цена в рублях',
        sysName: 'price_rub',
        placeholder: '123123',
        disabled:false,

    },
    {
        label: 'Цена в USDT',
        sysName: 'price_usdt',
        placeholder: '43243423',
        disabled:false,
    },
    {
        label: 'Спальни',
        sysName: 'bedrooms',
        placeholder: '1'
    },
    {
        label: 'Этажность',
        sysName: 'floors',
        placeholder: '1'
    },
    {
        label: 'Площадь',
        sysName: 'square',
        placeholder: '1'
    },
    {
        label: 'Тип Объекта',
        sysName: 'house_type',
        input: 'select'
    },
    {
        label: 'Поселок',
        sysName: 'settlement',
        input: 'select'
    },

]