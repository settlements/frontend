import {defineStore} from 'pinia';
import {ref} from 'vue';
import {useLocalStorage} from '@vueuse/core';
import user from '../api/user.js';

const userStore = defineStore('userStore', () => {
  const store = useLocalStorage('user', {userName: '', token: '', meta: {}});
  const storeLocal = ref(store.value);

  if (Boolean(storeLocal.value.token) !== false) {
    user.setAuthorizationToken(storeLocal.value.token);
  }
  const setInfo = (name = '', tokenString = '', info = {}) => {
    storeLocal.value = {userName: name, token: tokenString, meta: info};
    store.value = storeLocal.value;
  };

  const setSorting = (sortType) => {
    storeLocal.value.meta.sort = sortType;
    store.value = storeLocal.value;
  };

  const setUserToken = (token) => {
    storeLocal.value.token = token;
    user.setAuthorizationToken(token);
  };
  const logout = () => {
    store.value  =  {userName: '', token: '', meta: {}};
    storeLocal.value = store.value;
  }

  return {user: storeLocal, setInfo, setSorting,setUserToken, logout};
});

export {userStore};