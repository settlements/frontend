import {defineStore} from 'pinia';
import {ref} from  'vue';

const currencyPriceStore = defineStore('currencyPrice', () => {
  const  currencyPrice = ref(new Map());

  const setCurrency = (name,price) => currencyPrice.value.set(name,price);
  const getCurrency = (name) => currencyPrice.value.get(name);

  return {currencyPrice,setCurrency,getCurrency};
});

export {currencyPriceStore};