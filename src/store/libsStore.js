import {defineStore} from 'pinia';
import {ref} from  'vue';

const libsStore = defineStore('libsStore', () => {
  const  libs = ref(new Map());

  const setLibs = (name,lib) => libs.value.set(name,lib);
  const getLibs = (name) => libs.value.get(name);
  const getNameById = (name,idSearch) => libs.value.get(name).find(({id}) => id == idSearch).name

  return {libs,setLibs,getLibs,getNameById};
});

export {libsStore};