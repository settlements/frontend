import {inject, onMounted} from 'vue';

export function setPageTitle(title) {
  onMounted(() => {
    const pageTitle = inject('pageTitle');
    pageTitle.value = title;
  });
}

export function formatPrice(value) {
  let val = (value / 1).toFixed(2).replace('.', ',');
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}