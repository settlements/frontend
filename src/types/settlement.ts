import {File} from "filepond";

declare  type fieldName = 'name' | 'address' | 'square' | 'phone' | 'youtube';

export interface settlementFieldsInterface {
    label: string,
    sysName: fieldName,
    placeholder: string
}

interface settlementPropsDefaultInterface {
    name: string,
    address: string,
    square: number | string,
    phone: string,
    youtube: string,
    photo: string | File,
    presentation: string | File,
}

interface settlementPropsInterface extends settlementPropsDefaultInterface {
    photo: string,
    presentation: string,
}

export interface saveSettlement extends settlementPropsDefaultInterface {
    photo: File, //TODO rewrite to file
    presentation: File,  //TODO rewrite to file
}

export  type settlement = settlementPropsInterface | {};
